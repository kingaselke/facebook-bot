<?php
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Cache\SymfonyCache;
use Symfony\Component\Cache\Adapter;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;

require __DIR__.'/vendor/autoload.php';

$config = [
    'facebook' => [
        'token' => 'EAACNYSf2AHQBAIw8HVVFsZBXmPa5mKmyQ06laDhOqWgoaPxKDMcvKWPhzyw8RMu5kcsqUPJXUE3YvfCtkfS2AAblxndYZAQZAvnZB22QZAsFzVgZACteTOYEtQ5eu4lS5uEmMmnjBQYPR6rRi33njvypa1ATptTvpdFlFFBqbmXHBtzZCx7OW3J',
        'app_secret' => 'a41a3b31a252e950ae9c64bc282903b7',
        'verification'=>'example-token',
    ]
];


// Load the driver(s) you want to use
DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookDriver::class);

// Create an instance
$adapter = new Adapter\FilesystemAdapter();
$botman = BotManFactory::create($config, new SymfonyCache($adapter));




//$botman = BotManFactory::create($config);
////
//$botman->hears('I hello ([0-9]+)', function (BotMan $bot, $number) {
//    $bot->reply('You will hello: '.$number);



$botman->hears('hello|halo|hej|cześć|siema|elo|halo|czesc|hej kapustka| cześć kapustka', function (BotMan $bot) {
    require __DIR__ . '/src/DbRodzina.php';
    $userName = $bot->getUser()->getFirstName();
    if ($userName == 'Kinga') {
        $rodzina = new src\DbRodzina();
        $age = $rodzina->getPersonAge();

        $bot->reply('You are ' . $age . ' years old');
        $bot->startConversation(new OnBoardingConversation2());

    } else {
        $bot->reply('Hi ' . $userName . ' I\'m Kapustka! nice to meet you');
        $bot->startConversation(new OnBoardingConversation());
    }
});

$botman->hears( 'kapustka| I love you | thx', function(BotMan $bot) {

    $bot->reply('<3');


});

class OnBoardingConversation2 extends Conversation
{
    public function run()
    {
        // This will be called immediately
        $this->helloToJoanna();
    }

    public function helloToJoanna()
    {

        $attachment = new Image('https://media.giphy.com/media/FrnpqArQZtti8/giphy.gif');

        // Build message object
        $message = OutgoingMessage::create('This is my text')
            ->withAttachment($attachment);
        $this->say('Woooow, Joanna !');

        $this->say($message);
        $this->doYouWantABaloon();
    }
    public function doYouWantABaloon()
    {
        $this->ask("Do you want a bulik dog ?", [
            [
                'pattern' => 'yes|tak|pewnie|jasne|yep|yea|ya|ok|sure|why not|love one|alright',
                'callback' => function () {
                    $this->say("alright.. you asked for it!");

                    $attachment = new Image('https://www.psy.pl/wp-content/uploads/2017/11/shutterstock_420755203-e1509492356529.jpg');

                    // Build message object
                    $message = OutgoingMessage::create('This is my text')
                        ->withAttachment($attachment);


                    $this->say($message);
                    $this->repeat('bye !! :) ');

                }


            ],

            [

                'callback' => function () {

                    $attachment = new Image('https://media.giphy.com/media/lBm6rHWoBEpaw/giphy.gif');

                    // Build message object
                    $message = OutgoingMessage::create('This is my text')
                        ->withAttachment($attachment);

                    $this->say($message);


                }
            ]
        ]);
    }
}

class OnBoardingConversation extends Conversation
{

    protected $firstname;

    protected $email;


    public function doYouWantABaloon()
    {
        $this->ask("Do you want a bulik dog, {$this->firstname}?", [
            [
                'pattern' => 'yes|tak|pewnie|jasne|yep|yea|ya|ok|sure|why not|love one|alright',
                'callback' => function () {
                    $this->say("alright.. you asked for it!");

                    $attachment = new Image('https://www.psy.pl/wp-content/uploads/2017/11/shutterstock_420755203-e1509492356529.jpg');

                    // Build message object
                    $message = OutgoingMessage::create('This is my text')
                        ->withAttachment($attachment);


                    $this->say($message);
                    $this->repeat('bye !! :) ');

                }


            ],

            [

            'callback' => function () {

                $attachment = new Image('https://media.giphy.com/media/lBm6rHWoBEpaw/giphy.gif');

                // Build message object
                $message = OutgoingMessage::create('This is my text')
                    ->withAttachment($attachment);

                $this->say($message);


            }
        ]
        ]);
    }

    public function balloon(){

    }
    public function askFirstname()
    {
        $this->ask('Hello! Tu Kapustka. What is your name ?', function (Answer $answer) {
            $this->firstname = $answer->getText();

            $this->say('Nice to meet you ' . $this->firstname);
            $this->doYouWantABaloon();
//        });
//
//
//        $this->ask('Hello! What is your firstname?', function(Answer $answer) {
//            // Save result
//            $this->firstname = $answer->getText();
//
//            $this->say('Nice to meet you '.$this->firstname);
            //  $this->askEmail();
        });
    }

    public function askEmail()
    {
        $this->ask('How old are you ? ', function(Answer $answer) {
            // Save result
            $this->email = $answer->getText();

            $this->say('Wow - you are so young , '.$this->firstname);
            $this->doYouWantABaloon();
        });
    }

    public function run()
    {
        // This will be called immediately
        $this->doYouWantABaloon();
    }
}
$botman->listen();

